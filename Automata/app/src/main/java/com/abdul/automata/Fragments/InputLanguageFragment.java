package com.abdul.automata.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.abdul.automata.InfoActivity;
import com.abdul.automata.Main;
import com.abdul.automata.R;


import java.util.ArrayList;

public class InputLanguageFragment extends Fragment implements View.OnClickListener {
    private Spinner dd_FA;
    private ArrayList<EditText> inputs;
    private ArrayList<ImageButton> btn_remove_list;
    private TableLayout ll_inputs;
    private int count=0,type=0,counter=0;
    private HomeFragmentListener listener;
    private Context context;
    public static String NAME="Home_Fragment";
    //Listener
    public interface HomeFragmentListener {
        public void getInputAndType(ArrayList<String> arrayList, int type);
    }


    //Functions
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.home_fragment,container,false);

        dd_FA= (Spinner) v.findViewById(R.id.dd_selectFA);
        Button btn_next = (Button) v.findViewById(R.id.btn_next);
        Button btn_info = (Button) v.findViewById(R.id.btn_info);
        Button btn_add = (Button) v.findViewById(R.id.btn_add);
        ll_inputs= (TableLayout) v.findViewById(R.id.ll_input);

        btn_add.setOnClickListener(this);
        btn_info.setOnClickListener(this);
        btn_next.setOnClickListener(this);
        dd_FA.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==2&&!inputs.get(0).getText().toString().equals("^"))
                {
                    TableRow.LayoutParams param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
                    TableRow tr = new TableRow(getActivity());
                    tr.setLayoutParams(param);
                    tr.setId(Integer.parseInt(count + ""));
                    tr.setWeightSum(1);

                    param = new TableRow.LayoutParams(0, 200, 0.15f);
                    param.gravity = Gravity.CENTER;
                    ImageButton btn_del = new ImageButton(getActivity());
                    btn_del.setLayoutParams(param);
                    btn_del.setImageResource(R.drawable.delete);
                    btn_del.setTag(count);
                    btn_del.setBackgroundColor(Color.TRANSPARENT);
                    btn_del.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    btn_del.setAdjustViewBounds(true);
                    btn_del.setEnabled(false);


                    param = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 0.85f);
                    param.gravity = Gravity.CENTER;
                    EditText et = new EditText(getActivity());
                    et.setLayoutParams(param);
                    et.setTag(count);
                    et.setText("^");
                    et.setEnabled(false);
                    inputs.add(0,et);
                    et.setHint("Input Symbol");
                    btn_remove_list.add(0,btn_del);
                    tr.addView(et);
                    tr.addView(btn_del);
                    ll_inputs.addView(tr,0);
                    count++;
                    counter++;
                }
                else
                {
                    if(type==2)
                    {
                        ll_inputs.removeViewAt(0);
                        btn_remove_list.remove(0);
                        inputs.remove(0);

                    }

                }
                type=position;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        SharedPreferences sp=context.getSharedPreferences(Main.F_NAME,0);
        int check=sp.getInt(NAME+" Displayed",0);
        if (check==1)
        {
            inputs = new ArrayList<EditText>();
            btn_remove_list = new ArrayList<ImageButton>();
            addLanguage();
            onPause();
        }
        else
        {
            inputs=null;
        }
        SharedPreferences.Editor editor=sp.edit();
        editor.putInt(NAME+Main.E_PART_B,++check);
        editor.commit();

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        this.context=activity;
        listener= (HomeFragmentListener) activity;
        super.onAttach(activity);
    }
    @Override
    public void onResume() {
        SharedPreferences preferences = context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
        load(preferences);
        super.onResume();
    }
    @Override
    public void onPause() {
        SharedPreferences preferences = context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
        save(preferences);
        super.onPause();
    }


    //making data persistent
    public void save(SharedPreferences dataStore){
        SharedPreferences.Editor editor = dataStore.edit();
        if (inputs!=null){
            int size=inputs.size();
            editor.putInt("size",counter);
            for (int i=0;i<size;i++) {
                editor.putString("input" + i, inputs.get(i).getText().toString());
            }
            editor.putInt("type",type);
        }
        editor.commit();
    }
    //loading data
    public void load(SharedPreferences dataStore){
        if(inputs==null){
            counter = dataStore.getInt("size",0);
            type=dataStore.getInt("type",0);
            inputs=new ArrayList<>();
            btn_remove_list=new ArrayList<>();
            setDD();
            for (int i = 0; i < counter; i++) {
                String s = dataStore.getString("input" + i, "");
                addLanguage(s);
            }
        }
    }
    //setting loaded given fa
    private void setDD() {
        dd_FA.setSelection(type);
    }
    //next button clicked call
    public void movingToNext()
    {
        ArrayList<String> inputList=new ArrayList<String>();
        for(int i=0;i<inputs.size();i++)
        {
            inputList.add(inputs.get(i).getText().toString());
        }
        onPause();
        listener.getInputAndType(inputList,type);
    }
    //deleting row
    void deleteBtn(View v)
    {
        if(btn_remove_list.size()>1){
            int i=0;
            for(;i<btn_remove_list.size();i++)
            {
                if(btn_remove_list.get(i).getTag()==v.getTag())
                {
                    break;
                }
            }
            inputs.remove(i);
            btn_remove_list.remove(i);
            ll_inputs.removeViewAt(i);
            counter--;
        }
        else
        {

        }
    }
    //click on buttons
    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btn_info)
        {
            startActivityForResult(new Intent(getActivity(),InfoActivity.class),0);
        }

        if(v.getId()==R.id.btn_add)
        {
            addLanguage();
        }
        else if(v.getId()==R.id.btn_next)
        {
            boolean check=true;
            int i=0;
            for(;i<inputs.size();i++)
            {
                if(inputs.get(i).getText().toString().equals(""))
                {
                    check=false;
                    break;
                }
            }
            if(check)
                movingToNext();
            else
                Toast.makeText(getActivity(),(i+1)+" th index input can't be empty.",Toast.LENGTH_SHORT).show();
        }
    }
    //adding totally new row
    private void addLanguage() {
        TableRow.LayoutParams param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT);
        TableRow tr=new TableRow(getActivity());
        tr.setLayoutParams(param);
        tr.setWeightSum(1);

        param=new TableRow.LayoutParams(0,200,0.15f);
        param.gravity = Gravity.CENTER;
        ImageButton btn_del=new ImageButton(getActivity());
        btn_del.setLayoutParams(param);
        btn_del.setImageResource(R.drawable.delete);
        btn_del.setTag(count);
        btn_del.setBackgroundColor(Color.TRANSPARENT);
        btn_del.setScaleType(ImageView.ScaleType.FIT_CENTER);
        btn_del.setAdjustViewBounds(true);
        btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteBtn(v);
            }
        });


        param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,0.85f);
        param.gravity = Gravity.CENTER;
        EditText et=new EditText(getActivity());
        et.setLayoutParams(param);
        et.setId(counter);
        et.setTag(count);
        inputs.add(et);
        et.setHint("Input Symbol");
        btn_remove_list.add(btn_del);
        tr.addView(et);
        tr.addView(btn_del);
        ll_inputs.addView(tr);
        count++;
        counter++;

    }
    //adding row with given input string
    private void addLanguage(String  s) {
        TableRow.LayoutParams param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT);
        TableRow tr=new TableRow(getActivity());
        tr.setLayoutParams(param);
        tr.setWeightSum(1);

        param=new TableRow.LayoutParams(0,200,0.15f);
        param.gravity = Gravity.CENTER;
        ImageButton btn_del=new ImageButton(getActivity());
        btn_del.setLayoutParams(param);
        btn_del.setImageResource(R.drawable.delete);
        btn_del.setTag(count);
        btn_del.setBackgroundColor(Color.TRANSPARENT);
        btn_del.setScaleType(ImageView.ScaleType.FIT_CENTER);
        btn_del.setAdjustViewBounds(true);
        btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteBtn(v);
            }
        });


        param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,0.85f);
        param.gravity = Gravity.CENTER;
        EditText et=new EditText(getActivity());
        et.setLayoutParams(param);
        et.setText(s);

        et.setTag(count);
        inputs.add(et);
        et.setHint("Input Symbol");
        btn_remove_list.add(btn_del);
        tr.addView(et);
        tr.addView(btn_del);
        ll_inputs.addView(tr);
        count++;
    }
    //clearing saved data
    public void clear(Context context)
    {
        SharedPreferences preferences = context.getSharedPreferences(NAME, 0);
        preferences.edit().clear().commit();
    }
}
