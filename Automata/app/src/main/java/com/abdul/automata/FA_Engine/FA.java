package com.abdul.automata.FA_Engine;

import java.util.ArrayList;

/**
 * Created by Abdul Rehman on 10/6/2016.
 */
public class FA {

    //data member
    ArrayList<State> states;
    ArrayList<String> inputLanguage;

    //member function
    public ArrayList<String> getInputLanguage() {
        return inputLanguage;
    }
    public ArrayList<State> getStates() {
        return states;
    }
    public void setInputLanguage(ArrayList<String> inputLanguage) {
        this.inputLanguage = inputLanguage;
    }
    public FA() {
        states=new ArrayList<State>();
        inputLanguage=new ArrayList<String>();
    }
    public void addInputLanguage(String i)
    {
        inputLanguage.add(i);
    }
    public void addTransition(String s1,String s2,String input)
    {
        State state1,state2;
        state1=exist(s1);
        if(state1!=null)
        {
            state2=exist(s2);
            if(state2!=null)
            {
                if(inputLanguage.contains(input))
                {
                    TransitionOut t=new TransitionOut(input);
                    t.setNextState(state2);
                    state1.addOutGoing(t);
                }
            }
        }
    }
    private State exist(String s1)
    {
        State state = null;

        for(int i=0;i<states.size();i++)
        {
            if(states.get(i).getStateName().equals(s1))
            {
                state=states.get(i);
                break;
            }
        }
        return state;
    }
    public void printFA()
    {
        for(int i=0;i<states.size();i++)
        {
            for (int j=0;j<inputLanguage.size();j++)
            {
                if(states.get(i).checkTransition(inputLanguage.get(j))!="~~"){
                System.out.print(states.get(i).getStateName());
                System.out.print("\t"+states.get(i).isFinal());
                System.out.print("\t"+inputLanguage.get(j));
                System.out.println("\t"+states.get(i).checkTransition(inputLanguage.get(j)));
            }
            }
        }
    }
    public void eliminateState(String stateName) {
        State stateToBeDelete=exist(stateName);
        if(!states.get(0).equals(stateToBeDelete)&&!states.get(states.size()-1).equals(stateToBeDelete))
        {
            String self=stateToBeDelete.getSelf();
            ArrayList<TransitionOut> transOut=stateToBeDelete.getOutGoingRE(states);
            ArrayList<TransitionIn> transIn=stateToBeDelete.getInComingRE(states);
            if(self.length()!=0)
            {
                if(self.length()!=1)
                    self=" ("+self+")* ";
                else
                    self=self+"*";
            }

            for (int i=0;i<transIn.size();i++)
            {
                for (int j=0;j<transOut.size();j++)
                {
                    String s=new String();
                    String in=transIn.get(i).getInput(),out=transOut.get(j).getInput();
                    if(in.equals("^")||out.equals("^"))
                            if(in.equals("^")) {
                                if(out.length()!=1)
                                    s = self + "(" + out + ")";
                                else
                                    s = self + out;
                            }
                            if(out.equals("^")) {
                                if(in.length()!=1)
                                    s="("+in+")"+self;
                                else
                                    s=in+self;
                            }
                    else if(in.equals("^")&&out.equals("^"))
                            s=self;
                    else if (!in.equals("^")&&!out.equals("^")) {

                        if(in.length()!=1)
                            s="("+in+")";
                        else
                            s=in;
                        s+=self;
                        if(out.length()!=1)
                            s += "(" + out + ")";
                        else
                            s += out;
                    }
                    inputLanguage.add(s);
                    TransitionOut t=new TransitionOut(s);
                    t.setNextState(transOut.get(j).getNextState());
                    transIn.get(i).getPreviousState().addOutGoing(t);
                }
            }
            states.remove(stateToBeDelete);
            for(int i=0;i<states.size();i++)
            {
                states.get(i).removeAllTransition(stateToBeDelete);
            }

        }
        else
            {
                System.out.print("Not Possible");
            }
    }
    public void RECalculator() {
        State initial=getInitial();
        ArrayList<State> finalStates=getFinal();
        states.add(0,new State("Null",true,false));
        states.add(new State("Final",false,true));
        inputLanguage.add("^");

        TransitionOut t=new TransitionOut("^");
        t.setNextState(initial);
        states.get(0).addOutGoing(t);
        for(int i=0;i<finalStates.size();i++)
        {
            t=new TransitionOut("^");
            t.setNextState(states.get(states.size()-1));
            finalStates.get(i).addOutGoing(t);
        }
        int count=states.size()-1;
        for(int i=1;i<count;i++)
        {
            eliminateState(states.get(1).getStateName());
        }
    }
    State getInitial()
    {
        for (int i=0;i<states.size();i++)
        {
            if(states.get(i).isInitial())
            {
                return states.get(i);
            }
        }
        return null;
    }
    ArrayList<State> getFinal()
    {
        ArrayList<State> state=new ArrayList<State>();
        for (int i=0;i<states.size();i++)
        {
            if(states.get(i).isFinal())
            {
                state.add(states.get(i));
            }
        }
        return state;
    }
    public void addState(String q1, boolean isInitial, boolean isFinal) {
        State s=new State(q1,isInitial,isFinal);
        states.add(s);
    }
    public void addState(State s)
    {
        states.add(s);
    }

    public void getOutput(ArrayList<String> stateName, ArrayList<Boolean> isInitial, ArrayList<Boolean> isFinal, ArrayList<String> stateNameInTransitions, ArrayList<String> inputsInTransitions, ArrayList<String> toBeMovedStateInTransitions)
    {
        for(int i=0;i<states.size();i++)
        {
            stateName.add(states.get(i).getStateName());
            isInitial.add(states.get(i).isInitial());
            isFinal.add(states.get(i).isFinal());
        }
        for(int i=0;i<states.size();i++)
        {
            for (int j=0;j<inputLanguage.size();j++)
            {
                if(states.get(i).checkTransition(inputLanguage.get(j))!="~~"){
                    stateNameInTransitions.add(states.get(i).getStateName());
                    inputsInTransitions.add(inputLanguage.get(j));
                    toBeMovedStateInTransitions.add(states.get(i).checkTransition(inputLanguage.get(j)));
                }
            }
        }
    }

    public FA NFAtoDFA() {
        FA newFA=new FA();
        if(inputLanguage.contains("^"))
            inputLanguage.remove("^");
        newFA.setInputLanguage(inputLanguage);

        State s=states.get(0);
        newFA.addState(new State(s.getStateName(),s.isInitial(),s.isFinal()));
        newFA.addState("Trap",false,false);
        for (int i=0;i<newFA.getInputLanguage().size();i++)
            newFA.addTransition("Trap","Trap",newFA.getInputLanguage().get(i));


        for(int x=0;x<inputLanguage.size();x++) {
            ArrayList<State> statesOfNew = this.getStates().get(0).getNextStates(inputLanguage.get(x));
            String str = new String();
            State multipleState = new MultipleState();
            for (int i = 0; i < statesOfNew.size(); i++) {
                State q=statesOfNew.get(i);
                if(q.isFinal())
                    multipleState.setFinal(true);
                multipleState.addState(q);
                str += statesOfNew.get(i).getStateName();
                int e=i;
                e++;
                if(e!=statesOfNew.size())
                    str+=" ";
            }
            if(!str.equals("")){
            State check = newFA.exist(str);
            if(check==null) {
                multipleState.setStateName(str);
                newFA.addState(multipleState);
            }
            else
            {
                multipleState=check;
            }
            newFA.addTransition(newFA.getStates().get(0).getStateName(), multipleState.getStateName(), inputLanguage.get(x));
            }
        }
        for (int p=2;p<newFA.getStates().size();p++)
        {
            for(int x=0;x<newFA.getInputLanguage().size();x++) {
                ArrayList<State> statesOfNew = newFA.getStates().get(p).getNextStates(inputLanguage.get(x));
                String str = new String();
                State multipleState = new MultipleState();
                for (int i = 0; i < statesOfNew.size(); i++) {
                    State q=statesOfNew.get(i);
                    if(q.isFinal())
                        multipleState.setFinal(true);
                    multipleState.addState(q);
                    str += statesOfNew.get(i).getStateName() ;
                    int e=i;
                    e++;
                    if(e!=statesOfNew.size())
                        str+=" ";
                }
                if(!str.equals("")){
                State check = newFA.exist(str);
                if(check==null) {
                    multipleState.setStateName(str);
                    newFA.addState(multipleState);
                }
                else
                {
                    multipleState=check;
                }
                newFA.addTransition(newFA.getStates().get(p).getStateName(), multipleState.getStateName(), inputLanguage.get(x));
                }
            }
        }
        ArrayList<Integer> index=new ArrayList<Integer>();
        for(int i=0;i<newFA.getStates().size();i++)
        {
            newFA.getStates().get(i).setTrapToIncompleteState(newFA.getInputLanguage(),newFA.getStates().get(1));
        }
        return newFA;
    }

    public FA NFANullToNFA()
    {
        //creating new FA
        FA newFA=new FA();
        //setting same language in new FA
        newFA.setInputLanguage(inputLanguage);
        //adding null in new FA
        if(!newFA.getInputLanguage().contains("^"))
            newFA.getInputLanguage().add("^");
        // Adding all state in new FA
        State s=null;
        for(int i=0;i<states.size();i++)
        {
            s=states.get(i);
            newFA.addState(s.getStateName(),s.isInitial(),s.isFinal());
        }

        for(int p=0;p<newFA.getStates().size();p++) {
            for (int y = 0; y < newFA.getInputLanguage().size(); y++)
            {
                //getting null closure of states
                ArrayList<State> statesUponNull = states.get(p).getNullClosure("^");
                //checking whether initial take to final on null
                if(p==0)
                {
                    for (int i = 0; i < statesUponNull.size(); i++) {
                        if(statesUponNull.get(i).isFinal())
                        {
                            newFA.getStates().get(0).isFinal=true;
                        }
                    }
                }
                //applying transition function
                ArrayList<State> afterTransition = new ArrayList<State>();
                for (int x = 0; x < statesUponNull.size(); x++) {

                    String str = newFA.getInputLanguage().get(y);
                    if (!str.equals("^")) {
                        ArrayList<State> st = statesUponNull.get(x).getNextStates(str);
                        for (int j = 0; j < st.size(); j++) {
                            if (!afterTransition.contains(st.get(j))) {
                                afterTransition.add(st.get(j));
                            }
                        }
                    }
                }
                //getting null closure
                ArrayList<State> finalStatesUponNull = new ArrayList<State>();
                for (int i = 0; i < afterTransition.size(); i++) {
                    ArrayList<State> check = afterTransition.get(i).getNullClosure("^");
                    for (int j = 0; j < check.size(); j++) {
                        if (!finalStatesUponNull.contains(check.get(j))) {
                            finalStatesUponNull.add(check.get(j));
                        }
                    }
                }
                //adding transitions on states
                for (int i = 0; i < finalStatesUponNull.size(); i++) {
                    newFA.addTransition(newFA.getStates().get(p).getStateName(), finalStatesUponNull.get(i).getStateName(), newFA.getInputLanguage().get(y));
                }
            }
        }
        return newFA;
    }
    private ArrayList<State> check(State s,String c)
    {
        ArrayList<TransitionOut> arrayList= s.exist(c);
        ArrayList<State> states=null;
        for (int i=0;i<arrayList.size();i++)
        {
             states=new ArrayList<>();
            if (s.equals(arrayList.get(i).getNextState()))
                states.add(arrayList.get(i).getNextState());
        }
        return states;
    }
    public boolean computationalTree(String str)
    {
        ArrayList<State> states=new ArrayList<>();
        for (int i=0;i<this.states.size();i++)
        {
            if (this.states.get(i).isInitial())
            {
                states.add(this.states.get(i));
                break;
            }
        }
        for (int i=0;i<str.length();i++)
        {
            for (int j=0;j<states.size();j++)
            {
                ArrayList<State> list=check(states.get(j), String.valueOf(str.charAt(i)));
                if (list!=null) {
                    if (list.size() == 0) {
                        if (states.get(j).isFinal())
                            return true;
                    }
                    else
                    {
                        for (int z=0;z<list.size();z++)
                            states.add(list.get(z));
                    }
                }
            }
        }
        return false;
    }
}
