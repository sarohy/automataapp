package com.abdul.automata.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;


import com.abdul.automata.Main;
import com.abdul.automata.R;

import java.util.ArrayList;

public class InputStateFragment extends Fragment implements View.OnClickListener {
    public static final String NAME ="State_Fragment" ;
    TableLayout ll_states;
    ArrayList <RadioButton> initialStateList;
    ArrayList<EditText> stateList;
    ArrayList<CheckBox> finalStateList;
    ArrayList<ImageButton> btnDeleteList;
    Button btn_add;
    int tag=0,counter=0,rb_id=0,et_id=1000,cb_id=2000;
    StateFragmentListener listener;
    Button btn_next;
    private Context context;

    public interface StateFragmentListener
    {
        public void getStatesStatus(ArrayList<String> name, ArrayList<Boolean> isInitial, ArrayList<Boolean> isFinal);
    }
    //Function
    @Override
    public void onAttach(Activity activity) {
        this.context=activity;;
        listener= (StateFragmentListener) activity;
        super.onAttach(activity);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.state_fragment,container,false);
        btn_add= (Button) v.findViewById(R.id.btn_add);
        btn_next= (Button) v.findViewById(R.id.btn_next);
        ll_states= (TableLayout) v.findViewById(R.id.LL_state);
        btn_add.setOnClickListener(this);
        btn_next.setOnClickListener(this);

        SharedPreferences sp=context.getSharedPreferences(Main.F_NAME,0);
        int check=sp.getInt(NAME+" Displayed",0);
        if (check==1)
        {
            initialStateList=new ArrayList<RadioButton>();
            stateList=new ArrayList<EditText>();
            finalStateList=new ArrayList<CheckBox>();
            btnDeleteList=new ArrayList<ImageButton>();
            addState();
            onPause();
        }
        else
        {
            stateList=null;
        }
        SharedPreferences.Editor editor=sp.edit();
        editor.putInt(NAME+Main.E_PART_B,++check);
        editor.commit();
        return v;
    }





    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btn_add)
        {
            addState();
        }
        else if(v.getId()==R.id.btn_next)
        {
            boolean checkStateName=true,checkInitial=false,checkFinal=false;
            int i=0,j=0;
            for(;i<stateList.size();i++)
            {
                if(stateList.get(i).getText().toString().equals(""))
                {
                    checkStateName=false;
                    break;
                }
            }

            for(j=0;j<initialStateList.size();j++)
            {
                if(initialStateList.get(j).isChecked())
                {
                    checkInitial=true;
                    break;
                }
            }
            for(j=0;j<finalStateList.size();j++)
            {
                if(finalStateList.get(j).isChecked())
                {
                    checkFinal=true;
                    break;
                }
            }
            if(checkInitial&&checkStateName&&checkFinal)
                movingToNext();
            else
            {
                if(!checkStateName)
                    Toast.makeText(getActivity(),"State Name can't be empty.",Toast.LENGTH_SHORT).show();
                else if (!checkFinal)
                    Toast.makeText(getActivity(),"There should be at least one Final State",Toast.LENGTH_SHORT).show();
                else if (!checkInitial)
                    Toast.makeText(getActivity(),"There should be one initial State",Toast.LENGTH_SHORT).show();
            }
        }
    }
    @Override
    public void onResume() {
        SharedPreferences preferences = context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
        load(preferences);
        super.onResume();
    }
    @Override
    public void onPause() {
        SharedPreferences preferences = context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
        save(preferences);
        super.onPause();
    }

    //selecting any radio button
    void radioButtonClick(View v) {
        for(int i=0;i<initialStateList.size();i++)
        {
            if(initialStateList.get(i).getTag()!=v.getTag())
            {
                initialStateList.get(i).setChecked(false);
            }
        }
    }
    //clearing saved data
    public void clear(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, 0);
        preferences.edit().clear().commit();
    }
    //saving data
    public void save(SharedPreferences dataStore){
        SharedPreferences.Editor editor = dataStore.edit();
        if (stateList!=null) {
            editor.putInt("size", counter);
            for (int i = 0; i < counter; i++) {
                editor.putString("stateList" + i, stateList.get(i).getText().toString());
                editor.putBoolean("initialStateList" + i, initialStateList.get(i).isChecked());
                editor.putBoolean("finalStateList" + i, finalStateList.get(i).isChecked());
            }
        }
        editor.commit();
    }
    //loading data
    public void load(SharedPreferences dataStore){
        if (stateList==null) {
            initialStateList=new ArrayList<RadioButton>();
            stateList=new ArrayList<EditText>();
            finalStateList=new ArrayList<CheckBox>();
            btnDeleteList=new ArrayList<ImageButton>();
            counter = dataStore.getInt("size", 0);
            for (int i = 0; i < counter; i++) {
                String stateName = dataStore.getString("stateList" + i, "");
                boolean isInitial = dataStore.getBoolean("initialStateList" + i, false);
                boolean isFinal = dataStore.getBoolean("finalStateList" + i, false);
                addState(stateName, isInitial, isFinal);
            }
        }
    }
    //deleting row
    void deleteButtonClick(View v) {
        if(btnDeleteList.size()>1){
            int i=0;
            for(;i<btnDeleteList.size();i++)
            {
                if(btnDeleteList.get(i).getTag()==v.getTag())
                {
                    break;
                }
            }
            stateList.remove(i);
            btnDeleteList.remove(i);
            finalStateList.remove(i);
            initialStateList.remove(i);
            ll_states.removeViewAt(i+2);
            counter--;
        }
        else
        {

        }
    }
    //adding row with given state
    private void addState(String stateName, boolean isInitial, boolean isFinal) {
        TableRow.LayoutParams param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT);
        param.gravity = Gravity.CENTER;
        TableRow tr=new TableRow(getActivity());
        tr.setLayoutParams(param);
        tr.setWeightSum(3f);
        tr.setTag(tag);

        param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,0.5f);
        param.gravity = Gravity.CENTER;
        RadioButton rb=new RadioButton(getActivity());
        rb.setLayoutParams(param);
        rb.setId(rb_id);
        rb.setTag(tag);
        rb.setChecked(isInitial);
        rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioButtonClick(v);
            }
        });
        tr.addView(rb);
        initialStateList.add(rb);


        param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,1.5f);
        param.gravity = Gravity.CENTER;
        EditText et=new EditText(getActivity());
        et.setTag(tag);
        et.setId(et_id);
        et.setHint("State Name");
        et.setText(stateName);
        et.setLayoutParams(param);
        tr.addView(et);
        stateList.add(et);

        param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,0.5f);
        param.gravity = Gravity.CENTER;
        CheckBox cb=new CheckBox(getActivity());
        cb.setTag(tag);
        cb.setId(cb_id);
        cb.setChecked(isFinal);
        cb.setLayoutParams(param);
        finalStateList.add(cb);
        tr.addView(cb);

        param=new TableRow.LayoutParams(0,200,0.5f);
        param.gravity = Gravity.CENTER;
        ImageButton btn=new ImageButton(getActivity());
        btn.setTag(tag);
        btn.setLayoutParams(param);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteButtonClick(v);
            }
        });
        btn.setScaleType(ImageView.ScaleType.FIT_CENTER);
        btn.setAdjustViewBounds(true);
        btn.setImageResource(R.drawable.delete);
        btn.setTag(tag);
        btn.setBackgroundColor(Color.TRANSPARENT);
        btnDeleteList.add(btn);
        tr.addView(btn);


        ll_states.addView(tr);
        tag++;
        rb_id++;
        cb_id++;
        et_id++;

    }
    //adding row
    private void addState() {
        TableRow.LayoutParams param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT);
        param.gravity = Gravity.CENTER;
        TableRow tr=new TableRow(getActivity());
        tr.setLayoutParams(param);
        tr.setWeightSum(3f);
        tr.setTag(tag);

        param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,0.5f);
        param.gravity = Gravity.CENTER;
        RadioButton rb=new RadioButton(getActivity());
        rb.setLayoutParams(param);
        rb.setId(rb_id);
        rb.setTag(tag);
        rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioButtonClick(v);
            }
        });
        tr.addView(rb);
        initialStateList.add(rb);


        param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,1.5f);
        param.gravity = Gravity.CENTER;
        EditText et=new EditText(getActivity());
        et.setTag(tag);
        et.setId(et_id);
        et.setHint("State Name");
        et.setLayoutParams(param);
        tr.addView(et);
        stateList.add(et);

        param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,0.5f);
        param.gravity = Gravity.CENTER;
        CheckBox cb=new CheckBox(getActivity());
        cb.setTag(tag);
        cb.setId(cb_id);
        cb.setLayoutParams(param);
        finalStateList.add(cb);
        tr.addView(cb);

        param=new TableRow.LayoutParams(0,200,0.5f);
        param.gravity = Gravity.CENTER;
        ImageButton btn=new ImageButton(getActivity());
        btn.setTag(tag);
        btn.setLayoutParams(param);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteButtonClick(v);
            }
        });
        btn.setScaleType(ImageView.ScaleType.FIT_CENTER);
        btn.setAdjustViewBounds(true);
        btn.setImageResource(R.drawable.delete);
        btn.setTag(tag);
        btn.setBackgroundColor(Color.TRANSPARENT);
        btnDeleteList.add(btn);
        tr.addView(btn);


        ll_states.addView(tr);
        tag++;
        rb_id++;
        cb_id++;
        et_id++;
        counter++;

    }
    //clicking on next button
    public void movingToNext() {
        ArrayList<String> name=new ArrayList<String>();
        ArrayList<Boolean> isInitial=new ArrayList<Boolean>();
        ArrayList<Boolean> isFinal=new ArrayList<Boolean>();

        for(int i=0;i<stateList.size();i++)
        {
            name.add(stateList.get(i).getText().toString());
            isFinal.add(finalStateList.get(i).isChecked());
            isInitial.add(initialStateList.get(i).isChecked());
        }
        listener.getStatesStatus(name,isInitial,isFinal);
    }
}
