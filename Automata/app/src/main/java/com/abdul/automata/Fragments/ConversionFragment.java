package com.abdul.automata.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.abdul.automata.Main;
import com.abdul.automata.R;

import java.util.ArrayList;

public class ConversionFragment extends Fragment
{
    public static final String NAME = "Conversion_Fragment" ;
    ArrayList<String> stateName;
    ArrayList<Boolean> isInitial;
    ArrayList<Boolean> isFinal;
    ArrayList<String> stateNameInTransitions;
    ArrayList<String> inputsInTransitions;
    ArrayList<String> toBeMovedStateInTransitions;
    TableLayout tl_transitionTable,tl_stateTable;
    int type=-1,conversionType;
    LinearLayout ll_conversion;
    ConversionFragmentListener listener;
    private Context context;


    public interface ConversionFragmentListener
    {
       public void getConversionType(int conversionType);
    }

    //Functions
    @Override
    public void onAttach(Activity activity) {
        context=activity;
        listener= (ConversionFragmentListener) activity;
        super.onAttach(activity);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.conversion_fragment,container,false);
        tl_stateTable= (TableLayout) v.findViewById(R.id.tl_stateTable);
        tl_transitionTable= (TableLayout) v.findViewById(R.id.tl_transitionTable);
        ll_conversion= (LinearLayout) v.findViewById(R.id.ll_conversion);

        SharedPreferences sp=context.getSharedPreferences(Main.F_NAME,0);
        int check=sp.getInt(NAME+" Displayed",0);
        if (check==1)
        {
            init_conversion();
            init_transitionTable();
            init_stateTable();
            onPause();
        }
        else
        {
            stateName=null;
            stateNameInTransitions=null;
        }
        SharedPreferences.Editor editor=sp.edit();
        editor.putInt(NAME+Main.E_PART_B,++check);
        editor.commit();
        return v;
    }
    @Override
    public void onResume() {
        SharedPreferences preferences = context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
        load(preferences);
        super.onResume();
    }
    @Override
    public void onPause() {
        SharedPreferences preferences = context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
        save(preferences);
        super.onPause();
    }

    //Adding Buttons
    private void init_conversion() {
        if(type!=-1) {
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT
                    , LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 15, 0, 0);
            Button btn_RE = new Button(getActivity());
            btn_RE.setBackgroundColor(Color.rgb(2, 195, 154));
            btn_RE.setTextColor(Color.WHITE);
            btn_RE.setTextSize(20f);
            btn_RE.setLayoutParams(lp);
            btn_RE.setText("RE(Regular Expression)");
            btn_RE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    conversionType = 0;
                    listener.getConversionType(conversionType);
                }
            });
            Button btn_DFA = new Button(getActivity());
            btn_DFA.setText("DFA");
            btn_DFA.setLayoutParams(lp);
            btn_DFA.setTextColor(Color.WHITE);
            btn_DFA.setTextSize(20f);
            btn_DFA.setBackgroundColor(Color.rgb(2, 195, 154));
            btn_DFA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    conversionType = 1;
                    listener.getConversionType(conversionType);
                }
            });
            Button btn_NFA = new Button(getActivity());
            btn_NFA.setText("NFA");
            btn_NFA.setTextColor(Color.WHITE);
            btn_NFA.setTextSize(20f);
            btn_NFA.setLayoutParams(lp);
            btn_NFA.setBackgroundColor(Color.rgb(2, 195, 154));
            btn_NFA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    conversionType = 2;
                    listener.getConversionType(conversionType);
                }
            });

            if (type == 0) {
                ll_conversion.addView(btn_RE);
            }
            if (type == 1) {
                ll_conversion.addView(btn_RE);
                ll_conversion.addView(btn_DFA);
            }
            if (type == 2) {
                ll_conversion.addView(btn_RE);
                ll_conversion.addView(btn_DFA);
                ll_conversion.addView(btn_NFA);
            }
        }
    }
    //Making Transition Table
    private void init_transitionTable() {
        if (stateNameInTransitions!=null) {
            for (int i = 0; i < stateNameInTransitions.size(); i++) {
                TableRow.LayoutParams param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
                param.gravity = Gravity.CENTER;
                TableRow tr = new TableRow(getActivity());
                tr.setLayoutParams(param);

                param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
                param.gravity = Gravity.CENTER;
                TextView tv = new TextView(getActivity());
                tv.setLayoutParams(param);
                tr.addView(tv, 0);
                tv.setText(stateNameInTransitions.get(i));

                param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
                param.gravity = Gravity.CENTER;
                TextView tv1 = new TextView(getActivity());
                tv1.setLayoutParams(param);
                tr.addView(tv1, 1);
                tv1.setText(inputsInTransitions.get(i));

                param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
                param.gravity = Gravity.CENTER;
                TextView tv2 = new TextView(getActivity());
                tv2.setLayoutParams(param);
                tr.addView(tv2, 2);
                tv2.setText(toBeMovedStateInTransitions.get(i));

                tl_transitionTable.addView(tr);

            }
        }
    }
    //Making State Table
    private void init_stateTable() {
        if(stateName!=null){
            for(int i=0;i<stateName.size();i++)
            {
                TableRow.LayoutParams param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT);
                param.gravity = Gravity.CENTER;
                TableRow tr=new TableRow(getActivity());
                tr.setLayoutParams(param);

                param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT);
                param.gravity = Gravity.CENTER;
                TextView tv=new TextView(getActivity());
                tv.setLayoutParams(param);
                tr.addView(tv,0);
                tv.setText(stateName.get(i));

                param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT);
                param.gravity = Gravity.CENTER;
                CheckBox cb=new CheckBox(getActivity());
                cb.setLayoutParams(param);
                cb.setEnabled(false);
                cb.setChecked(isInitial.get(i));
                tr.addView(cb,1);

                param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT);
                param.gravity = Gravity.CENTER;
                CheckBox cb1=new CheckBox(getActivity());
                cb1.setLayoutParams(param);
                cb1.setEnabled(false);
                cb1.setChecked(isFinal.get(i));
                tr.addView(cb1,2);

                tl_stateTable.addView(tr);

            }
        }
    }
    //Clearing save data
    public static void clear(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, 0);
        preferences.edit().clear().commit();
    }
    //Saving data
    public void save(SharedPreferences dataStore){
        SharedPreferences.Editor editor = dataStore.edit();
        if(stateName!=null) {
            editor.putInt("stateSize", stateName.size());
            for (int i = 0; i < stateName.size(); i++) {
                editor.putString("stateList" + i, stateName.get(i));
                editor.putBoolean("initialStateList" + i, isInitial.get(i));
                editor.putBoolean("finalStateList" + i, isFinal.get(i));
            }
        }
        if(stateNameInTransitions!=null) {
            editor.putInt("TransSize", stateNameInTransitions.size());
            for (int i = 0; i < stateNameInTransitions.size(); i++) {
                editor.putString("stateNameInTransitions" + i, stateNameInTransitions.get(i));
                editor.putString("inputsInTransitions" + i, inputsInTransitions.get(i));
                editor.putString("toBeMovedStateInTransitions" + i, toBeMovedStateInTransitions.get(i));
            }
            editor.putInt("conversionType", conversionType);
            editor.putInt("type", type);
        }
        editor.commit();
    }
    //loading data
    public void load(SharedPreferences dataStore){
        if(stateName==null){
            int counter = dataStore.getInt("stateSize",0);
            stateName=new ArrayList<>();
            isFinal=new ArrayList<>();
            isInitial=new ArrayList<>();
            for (int i = 0; i < counter; i++)
            {
                stateName.add(dataStore.getString("stateList" + i, ""));
                isInitial.add(dataStore.getBoolean("initialStateList" + i, false));
                isFinal.add(dataStore.getBoolean("finalStateList" + i, false));
            }
        }
        if(stateNameInTransitions==null){
            int counter = dataStore.getInt("TransSize",0);
            stateNameInTransitions=new ArrayList<>();
            inputsInTransitions=new ArrayList<>();
            toBeMovedStateInTransitions=new ArrayList<>();
            for (int i = 0; i < counter; i++)
            {
                stateNameInTransitions.add(dataStore.getString("stateNameInTransitions" + i, ""));
                inputsInTransitions.add(dataStore.getString("inputsInTransitions" + i, ""));
                toBeMovedStateInTransitions.add(dataStore.getString("toBeMovedStateInTransitions" + i, ""));
            }
            conversionType =dataStore.getInt("conversionType",0);
            type=dataStore.getInt("type",-1);
            init_conversion();
            init_stateTable();
            init_transitionTable();
        }
    }
    //init with data from Main
    public void setTransition(ArrayList<String> stateName, ArrayList<Boolean> isInitial, ArrayList<Boolean> isFinal,
                              ArrayList<String> stateNameInTransitions, ArrayList<String> inputsInTransitions,
                              ArrayList<String> toBeMovedStateInTransitions,int type) {
        this.type=type;
        this.stateName=stateName;
        this.isFinal=isFinal;
        this.isInitial=isInitial;
        this.stateNameInTransitions=stateNameInTransitions;
        this.inputsInTransitions=inputsInTransitions;
        this.toBeMovedStateInTransitions=toBeMovedStateInTransitions;
    }
}
