package com.abdul.automata.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abdul.automata.Main;
import com.abdul.automata.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;

/**
 * Created by Abdul on 10/24/2016.
 */
public class ReOutputFragment extends Fragment {
    public static final String NAME = "ReOutput_Fragment";
    LinearLayout ll_output;
    ArrayList<String> output;
    Button btn_new,btn_rate;
    ReOutputFragmentListener listener ;
    private Context context;
    private InterstitialAd mInterstitialAd;

    public interface ReOutputFragmentListener
    {
     public void newFA();
    }

    @Override
    public void onResume() {
        SharedPreferences preferences = context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
        load(preferences);
        super.onResume();
    }
    @Override
    public void onPause() {
        SharedPreferences preferences = context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
        save(preferences);
        super.onPause();
    }
    @Override
    public void onAttach(Activity activity) {
        this.context=activity;
        listener= (ReOutputFragmentListener) activity;
        super.onAttach(activity);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.reoutput_fragment, container, false);
        ll_output= (LinearLayout) v.findViewById(R.id.ll_output);
        btn_new= (Button) v.findViewById(R.id.btn_next);
        btn_rate= (Button) v.findViewById(R.id.btn_rateus);
        btn_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=" + context.getPackageName())));
                } catch (android.content.ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
                }
            }
        });
        btn_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.newFA();
            }
        });
        init();
        mInterstitialAd = new InterstitialAd(getActivity());
        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));

        AdRequest adRequest = new AdRequest.Builder()
                .build();

        // Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }
        });
        onPause();
        return v;
    }
    //saving data
    public void save(SharedPreferences dataStore){
        SharedPreferences.Editor editor = dataStore.edit();
        if(output!=null) {
            int size = output.size();
            editor.putInt("size", output.size());
            for (int i = 0; i < size; i++) {
                editor.putString("output" + i, output.get(i));
            }
        }
        editor.commit();
    }
    //loading data
    public void load(SharedPreferences dataStore){
        int counter = dataStore.getInt("size",0);
        if(output==null)
        {
            output=new ArrayList<>();
            for (int i = 0; i < counter; i++) {
                output.add(dataStore.getString("output" + i, ""));
            }
            init();
        }
    }
    public void clear(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, 0);
        preferences.edit().clear().commit();
    }
    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }
    //initialization all thing
    private void init() {
        int k=0;
        if(output!=null) {
            for (int i = 0; i < output.size(); i++) {
                TextView tv = new TextView(getActivity());
                String s = output.get(i);
                k = i;
                k++;
                if ((k != output.size()))
                    s = s + " +";
                tv.setText(s);
                tv.setTextSize(22f);
                ll_output.addView(tv);
            }
        }
    }
    //setting string
    public void setOutput(ArrayList<String> inputsInTransitions) {
        this.output = inputsInTransitions;
    }
}
