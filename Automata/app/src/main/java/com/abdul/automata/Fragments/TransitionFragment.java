package com.abdul.automata.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;


import com.abdul.automata.Main;
import com.abdul.automata.R;

import java.util.ArrayList;

/**
 * Created by Abdul on 10/21/2016.
 */
public class TransitionFragment extends Fragment implements View.OnClickListener {
    public static final String NAME = "Transition_fragment" ;
    TableLayout ll_transitions;
    ArrayList<Spinner> states,languages,toMoveState;
    ArrayList<ImageButton> btnDeleteList;
    Button btn_add;
    ArrayList<String> state,language;
    int count=0,counter=0,sp1=0,sp2=1000,sp3=3000;
    transitionFragmentListener listener;
    Button btn_next;
    ArrayAdapter<String> stateAdapter,languageAdapter;
    int type;
    private Context context;

    public interface transitionFragmentListener
    {
        public void getTransitionTable(ArrayList<String> stateNames, ArrayList<String> inputs, ArrayList<String> toBeMovedState);
    }


    @Override
    public void onAttach(Activity activity) {
        this.context=activity;
        listener= (transitionFragmentListener) activity;
        super.onAttach(activity);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.transition_fragment,container,false);


        btn_add= (Button) v.findViewById(R.id.btn_add);
        btn_next= (Button) v.findViewById(R.id.btn_next);
        ll_transitions= (TableLayout) v.findViewById(R.id.ll_transitions);

        SharedPreferences sp=context.getSharedPreferences(Main.F_NAME,0);
        int check=sp.getInt(NAME+" Displayed",0);
        if (check==1)
        {
            stateAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, state);
            // Specify the layout to use when the list of choices appears
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            languageAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, language);
            // Specify the layout to use when the list of choices appears
            languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            states=new ArrayList<Spinner>();
            languages=new ArrayList<Spinner>();
            toMoveState=new ArrayList<Spinner>();
            btnDeleteList=new ArrayList<ImageButton>();
            addTransition();
            onPause();
        }
        else
        {
            states=null;
        }
        SharedPreferences.Editor editor=sp.edit();
        editor.putInt(NAME+Main.E_PART_B,++check);
        editor.commit();




        btn_next.setOnClickListener(this);
        btn_add.setOnClickListener(this);

        return v;
    }
    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btn_add)
        {
            addTransition();
        }
        else if(v.getId()==R.id.btn_next)
        {
            movingToNext();
        }
    }
    @Override
    public void onResume() {
        SharedPreferences preferences = context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
        load(preferences);
        super.onResume();
    }
    @Override
    public void onPause() {
        SharedPreferences preferences = context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
        save(preferences);
        super.onPause();
    }

    //next button clicked
    void movingToNext() {
        ArrayList<String> stateNames=new ArrayList<String>();
        ArrayList<String> inputs=new ArrayList<String>();
        ArrayList<String> toBeMovedState=new ArrayList<String>();
        for(int i=0;i<states.size();i++)
        {
            stateNames.add(state.get(states.get(i).getSelectedItemPosition()));
            toBeMovedState.add(state.get(toMoveState.get(i).getSelectedItemPosition()));
            inputs.add(language.get(languages.get(i).getSelectedItemPosition()));
        }
        onPause();
        listener.getTransitionTable(stateNames,inputs,toBeMovedState);
    }
    //adding row with given state
    private void addTransition(Integer sp1, Integer sp2, Integer sp3) {
        TableRow.LayoutParams param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT);
        param.gravity = Gravity.CENTER;
        TableRow tr=new TableRow(getActivity());
        tr.setLayoutParams(param);
        tr.setWeightSum(4f);
        tr.setTag(count);

        param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,1.2f);
        param.gravity = Gravity.CENTER;
        Spinner dd=new Spinner(getActivity());
        dd.setLayoutParams(param);
        dd.setTag(count);
        dd.setAdapter(stateAdapter);
        dd.setSelection(sp1);
        tr.addView(dd);
        states.add(dd);


        param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,1.2f);
        param.gravity = Gravity.CENTER;
        Spinner dd1=new Spinner(getActivity());
        dd1.setTag(count);
        dd1.setLayoutParams(param);
        dd1.setAdapter(languageAdapter);
        dd1.setSelection(sp2);
        tr.addView(dd1);
        languages.add(dd1);

        param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,1.2f);
        param.gravity = Gravity.CENTER;
        Spinner dd2=new Spinner(getActivity());
        dd2.setTag(count);
        dd2.setAdapter(stateAdapter);
        dd2.setSelection(sp3);
        dd2.setLayoutParams(param);
        toMoveState.add(dd2);
        tr.addView(dd2);

        param=new TableRow.LayoutParams(0,200,0.4f);
        param.gravity = Gravity.CENTER;
        ImageButton btn=new ImageButton(getActivity());
        btn.setTag(count);
        btn.setLayoutParams(param);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteButtonClick(v);
            }
        });
        btn.setScaleType(ImageView.ScaleType.FIT_CENTER);
        btn.setAdjustViewBounds(true);
        btn.setImageResource(R.drawable.delete);
        btn.setTag(count);
        btn.setBackgroundColor(Color.TRANSPARENT);
        btnDeleteList.add(btn);
        tr.addView(btn);


        ll_transitions.addView(tr);
        count++;
        this.sp1++;
        this.sp2++;
        this.sp3++;
    }
    //clearing saved data
    public static void clear(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, 0);
        preferences.edit().clear().commit();
    }
    //saving data
    public void save(SharedPreferences dataStore){
        SharedPreferences.Editor editor = dataStore.edit();
        if (states!=null) {
            editor.putInt("size", counter);
            for (int i = 0; i < counter; i++) {
                editor.putInt("states" + i, states.get(i).getSelectedItemPosition());
                editor.putInt("languages" + i, languages.get(i).getSelectedItemPosition());
                editor.putInt("toMoveState" + i, toMoveState.get(i).getSelectedItemPosition());
            }
        }
        if (state!=null) {
            editor.putInt("stateSize", state.size());
            for (int i = 0; i < state.size(); i++) {
                editor.putString("stateName" + i, state.get(i));
            }
        }
        if (language!=null) {
            editor.putInt("languageSize", language.size());
            for (int i = 0; i < language.size(); i++) {
                editor.putString("language" + i, language.get(i));
            }
        }
        editor.commit();
    }
    //loading data
    public void load(SharedPreferences dataStore){
        SharedPreferences editor = dataStore;
        if(state==null){
            state=new ArrayList<>();
            int k=editor.getInt("stateSize",0);
            for (int i=0;i<k;i++) {
                state.add(editor.getString("stateName" + i,""));
            }
        }
        if(language==null) {
            language=new ArrayList<>();
            int k = editor.getInt("languageSize", 0);
            for (int i = 0; i < k; i++) {
                language.add(editor.getString("language" + i, ""));
            }
        }
        stateAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, state);
        // Specify the layout to use when the list of choices appears
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        languageAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, language);
        // Specify the layout to use when the list of choices appears
        languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        if (states==null)
        {
            states=new ArrayList<>();
            languages=new ArrayList<>();
            toMoveState=new ArrayList<>();
            btnDeleteList=new ArrayList<>();
            counter = editor.getInt("size", 0);
            for (int i = 0; i < counter; i++) {
                int index1 = editor.getInt("states" + i, 0);
                int index2 = editor.getInt("languages" + i, 0);
                int index3 = editor.getInt("toMoveState" + i, 0);
                addTransition(index1, index2, index3);
            }
        }
    }
    //geting info from main
    public  void setStateAndLanguage(ArrayList<String> states,ArrayList<String> language ,int type) {
        this.state=states;
        this.language=language;
        this.type=type;
    }
    //deleting row
    void deleteButtonClick(View v) {
        if(btnDeleteList.size()>1){
            int i=0;
            for(;i<btnDeleteList.size();i++)
            {
                if(btnDeleteList.get(i).getTag()==v.getTag())
                {
                    break;
                }
            }
            states.remove(i);
            btnDeleteList.remove(i);
            toMoveState.remove(i);
            languages.remove(i);
            ll_transitions.removeViewAt(i+2);
            counter--;
        }
        else
        {

        }
    }
    //adding row
    private void addTransition() {
        TableRow.LayoutParams param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT);
        param.gravity = Gravity.CENTER;
        TableRow tr=new TableRow(getActivity());
        tr.setLayoutParams(param);
        tr.setWeightSum(4f);
        tr.setTag(count);

        param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,1.2f);
        param.gravity = Gravity.CENTER;
        Spinner dd=new Spinner(getActivity());
        dd.setLayoutParams(param);
        dd.setTag(count);
        dd.setId(sp1);
        tr.addView(dd);
        dd.setAdapter(stateAdapter);
        states.add(dd);


        param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,1.2f);
        param.gravity = Gravity.CENTER;
        Spinner dd1=new Spinner(getActivity());
        dd1.setTag(count);
        dd1.setLayoutParams(param);
        dd1.setId(sp2);
        tr.addView(dd1);
        dd1.setAdapter(languageAdapter);
        languages.add(dd1);

        param=new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,1.2f);
        param.gravity = Gravity.CENTER;
        Spinner dd2=new Spinner(getActivity());
        dd2.setTag(count);
        dd2.setId(sp3);
        dd2.setAdapter(stateAdapter);
        dd2.setLayoutParams(param);
        toMoveState.add(dd2);
        tr.addView(dd2);

        param=new TableRow.LayoutParams(0,200,0.4f);
        param.gravity = Gravity.CENTER;
        ImageButton btn=new ImageButton(getActivity());
        btn.setTag(count);
        btn.setLayoutParams(param);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteButtonClick(v);
            }
        });
        btn.setScaleType(ImageView.ScaleType.FIT_CENTER);
        btn.setAdjustViewBounds(true);
        btn.setImageResource(R.drawable.delete);
        btn.setTag(count);
        btn.setBackgroundColor(Color.TRANSPARENT);
        btnDeleteList.add(btn);
        tr.addView(btn);


        ll_transitions.addView(tr);
        count++;
        sp1++;
        sp2++;
        sp3++;
        counter++;
    }
}

