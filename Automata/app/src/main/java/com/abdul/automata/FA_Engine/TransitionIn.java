package com.abdul.automata.FA_Engine;

/**
 * Created by Abdul Rehman on 10/6/2016.
 */
public class TransitionIn {

    String input;


    State previousState;


    public void setPreviousState(State previousState) {
        this.previousState = previousState;
    }

    public String getInput() {
        return input;
    }

    public State getPreviousState() {
        return previousState;
    }

    public TransitionIn(String input) {
        this.input = input;
    }
}
