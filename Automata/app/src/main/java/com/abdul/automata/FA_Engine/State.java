package com.abdul.automata.FA_Engine;

import java.util.ArrayList;

/**
 * Created by Abdul Rehman on 10/6/2016.
 */
public class State {
    public String getStateName() {
        return stateName;
    }
    public State(String stateName, boolean isInitial, boolean isFinal) {
        this.stateName = stateName;
        this.isInitial = isInitial;
        this.isFinal = isFinal;
        inComing=new ArrayList<TransitionIn>();
        outGoing=new ArrayList<TransitionOut>();
    }
    public State() {}
    public void addOutGoing(TransitionOut outGoing) {
        this.outGoing.add(outGoing);
        TransitionIn t=new TransitionIn(outGoing.getInput());
        t.setPreviousState(this);
        outGoing.getNextState().addInComing(t);
    }
    public void addInComing(TransitionIn inComing) {
        this.inComing.add(inComing);
    }
    public ArrayList<TransitionOut> exist(String c)
    {
        ArrayList<TransitionOut> transitionOut = new ArrayList<TransitionOut>();

        for(int i=0;i<outGoing.size();i++)
        {
            if(outGoing.get(i).getInput().equals(c))
            {
                transitionOut.add(outGoing.get(i));
            }
        }
        return transitionOut;
    }
    public String checkTransition(String c)
    {
        ArrayList<TransitionOut> t=exist(c);
        if(t.size()!=0)
        {
            String s=new String() ;
            for(int i=0;i<t.size();i++){
             s+=t.get(i).getNextState().getStateName();
               int e=i;
                e++;
                if(e!=t.size())
                {
                    s+=", ";
                }

            }
            return s;
        }
        else
            {
                return "~~";
            }
    }
    public String getSelf() {
        String s=new String();
        int k=0;
        ArrayList<TransitionOut> t= (ArrayList<TransitionOut>) outGoing.clone();
        for(int i=0;i<outGoing.size();i++)
        {
            if(outGoing.get(i).getNextState().equals(this))
            {
                s+=outGoing.get(i).getInput();
                t.remove(k);
                if(existOut(this,i))
                {
                    s+="+";
                }
            }
            else
                {
                    k++;
                }
        }
        outGoing=t;
        for(int i=0;i<inComing.size();i++)
        {
            if(inComing.get(i).getPreviousState().equals(this))
            {
                inComing.remove(i);
            }
        }
        return s;
    }
    public void removeAllTransition(State stateToBeDelete) {
        ArrayList<TransitionOut> t= (ArrayList<TransitionOut>) outGoing.clone();
        int k=0;
        for (int i=0;i<outGoing.size();i++)
        {
            if(outGoing.get(i).getNextState()==stateToBeDelete)
            {
                t.remove(k);
            }
            else
                k++;
        }
        outGoing=t;
        k=0;
        ArrayList<TransitionIn> h= (ArrayList<TransitionIn>) inComing.clone();
        for (int i=0;i<inComing.size();i++)
        {
            if(inComing.get(i).getPreviousState()==stateToBeDelete)
            {
                h.remove(k);
            }
            else
                k++;
        }
        inComing=h;
    }
    public boolean  existOut(State s,int j)
    {
        for(int i=j+1;i<outGoing.size();i++)
        {
            if(outGoing.get(i).getNextState().equals(s))
            {
                return true;
            }
        }
        return false;
    }
    public boolean  existIn(State s,int j)
    {
        for(int i=j+1;i<inComing.size();i++)
        {
            if(inComing.get(i).getPreviousState().equals(s))
            {
                return true;
            }
        }
        return false;
    }
    public ArrayList<TransitionOut> getOutGoingRE(ArrayList<State> states)
    {
        ArrayList<TransitionOut> t=new ArrayList<TransitionOut>();
        for(int i=0;i<states.size();i++)
        {
            String s=new String();
            int j=0;
            boolean check=false;
            for (j=0;j<outGoing.size();j++)
            {

                if(outGoing.get(j).getNextState().equals(states.get(i))&&!outGoing.get(j).getNextState().equals(this))
                {
                    s+=outGoing.get(j).getInput();
                    check=true;
                    if(existOut(states.get(i),j))
                        s+="+";
                }
            }
            if(check)
            {
            TransitionOut trans=new TransitionOut(s);
            trans.setNextState(states.get(i));
            t.add(trans);
            }
        }
        return t;
    }
    public ArrayList<TransitionIn> getInComingRE(ArrayList<State> states)
    {

        ArrayList<TransitionIn> t=new ArrayList<TransitionIn>();
        for(int i=0;i<states.size();i++)
        {
            String s=new String();
            int j=0;
            boolean check=false;
            for (j=0;j<inComing.size();j++)
            {
                if(inComing.get(j).getPreviousState().equals(states.get(i))&&!inComing.get(j).getPreviousState().equals(this))
                {
                    check=true;
                    s+=inComing.get(j).getInput();
                    if(existIn(states.get(i),j))
                        s+="+";
                }
            }
            if(check)
            {
            TransitionIn trans=new TransitionIn(s);
            trans.setPreviousState(states.get(i));
            t.add(trans);
            }
        }
        return t;
    }
    public boolean isInitial() {
        return isInitial;
    }
    public void setInitial(boolean initial) {
        isInitial = initial;
    }
    public boolean isFinal() {
        return isFinal;
    }
    public void setFinal(boolean aFinal) {
        isFinal = aFinal;
    }
    public ArrayList<State> getNextStates(String input)
    {
        ArrayList<State> states=new ArrayList<State>();
        for (int i=0;i<outGoing.size();i++)
        {
            if(outGoing.get(i).getInput().equals(input))
            {
                states.add(outGoing.get(i).getNextState());
            }
        }
        return  states;
    }
    public ArrayList<State> getNullClosure(String input)
    {
        ArrayList<State> states=new ArrayList<State>();
        states.add(this);
        for (int i = 0; i < states.size(); i++) {
            states.get(i).nullClosureList(input,states);
        }
        for (int i = 0; i < states.size(); i++) {
            if(states.get(i).isFinal())
            {
                this.isFinal=true;
            }
        }
        return  states;
    }
    public void nullClosureList(String input,ArrayList<State> s)
    {
        for (int i = 0; i < outGoing.size(); i++) {
            if (outGoing.get(i).getInput().equals(input))
            {
                if(!s.contains(outGoing.get(i).getNextState()))
                    s.add(outGoing.get(i).getNextState());
            }
        }
    }
    public void addState(State s)
    {
        this.stateName=s.getStateName();
        this.isInitial=s.isInitial;
        this.isFinal=s.isFinal;
        this.inComing=s.inComing;
        this.outGoing=s.outGoing;
    }
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
    public void setTrapToIncompleteState(ArrayList<String> inputLanguage, State state)
    {
        boolean check=false;
        for(int i=0;i<inputLanguage.size();i++)
        {
            check=false;
            for(int j=0;j<outGoing.size();j++)
            {
                if(outGoing.get(j).getInput().equals(inputLanguage.get(i)))
                {
                    check=true;
                }
            }
            if(!check)
            {
                TransitionOut t=new TransitionOut(inputLanguage.get(i));
                t.setNextState(state);
                this.addOutGoing(t);
            }
        }

    }

    String stateName;
    boolean isInitial;
    boolean isFinal;
    ArrayList<TransitionIn> inComing;
    ArrayList<TransitionOut> outGoing;


}
