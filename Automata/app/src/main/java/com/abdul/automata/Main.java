package com.abdul.automata;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.abdul.automata.FA_Engine.FA;
import com.abdul.automata.Fragments.ConversionFragment;
import com.abdul.automata.Fragments.InputLanguageFragment;
import com.abdul.automata.Fragments.InputStateFragment;
import com.abdul.automata.Fragments.ReOutputFragment;
import com.abdul.automata.Fragments.TransitionFragment;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

public class Main extends AppCompatActivity implements View.OnClickListener,InputLanguageFragment.HomeFragmentListener,InputStateFragment.StateFragmentListener,
        TransitionFragment.transitionFragmentListener,ConversionFragment.ConversionFragmentListener,ReOutputFragment.ReOutputFragmentListener
{
    private AdView mAdView;
    ArrayList<String> stateName=new ArrayList<>();
    ArrayList<Boolean> isInitial=new ArrayList<>();
    ArrayList<Boolean> isFinal=new ArrayList<>();
    ArrayList<String> inputLanguage=new ArrayList<>();
    ArrayList<String> stateNameInTransitions=new ArrayList<>();
    ArrayList<String> inputsInTransitions=new ArrayList<>();
    ArrayList<String> toBeMovedStateInTransitions=new ArrayList<>();
    int type,majorType;
    FA f;
    public static String NAME="Activity",F_NAME="Fragment",E_PART_B=" Displayed";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
        String tag = InputLanguageFragment.NAME;
        FragmentManager fragmentManager = getFragmentManager();
        if(fragmentManager.findFragmentByTag(tag) == null) {
            SharedPreferences sp=this.getSharedPreferences(F_NAME,0);
            SharedPreferences.Editor editor=sp.edit();
            editor.putInt(tag+E_PART_B,1);
            editor.commit();
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            InputLanguageFragment homeFragment = new InputLanguageFragment();
            homeFragment.clear(this);
            ft.setCustomAnimations(R.animator.fragment_slide_left_enter,
                    R.animator.fragment_slide_left_exit,
                    R.animator.fragment_slide_right_enter,
                    R.animator.fragment_slide_right_exit);
            ft.add(R.id.fragment_container, homeFragment, tag);
            ft.addToBackStack(tag);
            ft.commit();
        }
        f=new FA();
    }

    @Override
    protected void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }

        super.onDestroy();
    }

    @Override
    public void onResume() {
        if (mAdView != null) {
            mAdView.resume();
        }
        SharedPreferences preferences = this.getSharedPreferences(NAME,Context.MODE_PRIVATE);
        load(preferences);
        super.onResume();
    }
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        SharedPreferences preferences = this.getSharedPreferences(NAME,Context.MODE_PRIVATE);
        save(preferences);
        super.onPause();
    }
    //getting data from InputLanguage Fragment
    @Override
    public void getInputAndType(ArrayList<String> arrayList, int type) {
        //setting values
        this.inputLanguage=arrayList;
        this.type=type;
        this.majorType=type;
        //setting that fragment is opening for first time
        SharedPreferences sp=this.getSharedPreferences(F_NAME,0);
        SharedPreferences.Editor editor=sp.edit();
        String tag= InputStateFragment.NAME;
        editor.putInt(tag+E_PART_B,1);
        editor.commit();
        //Replacing fragment
        InputStateFragment newFragment = new InputStateFragment();
        newFragment.clear(this);
        FragmentManager transaction = getFragmentManager();
        FragmentTransaction fra = transaction.beginTransaction();
        fra.setCustomAnimations(R.animator.fragment_slide_left_enter,
                R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter,
                R.animator.fragment_slide_right_exit);
        fra.replace(R.id.fragment_container, newFragment);
        fra.addToBackStack(InputStateFragment.NAME);
        fra.commit();
        onPause();
    }
    //getting data from InputState Fragment
    @Override
    public void getStatesStatus(ArrayList<String> name, ArrayList<Boolean> isInitial, ArrayList<Boolean> isFinal) {
        //setting values
        this.stateName=name;
        this.isFinal=isFinal;
        this.isInitial=isInitial;
        //setting that fragment is opening for first time
        SharedPreferences sp=this.getSharedPreferences(F_NAME,0);
        SharedPreferences.Editor editor=sp.edit();
        String tag= TransitionFragment.NAME;
        editor.putInt(tag+E_PART_B,1);
        editor.commit();
        //Replacing fragment
        TransitionFragment newFragment = new TransitionFragment();
        TransitionFragment.clear(this);
        newFragment.setStateAndLanguage(stateName,inputLanguage,majorType);
        FragmentManager transaction = getFragmentManager();
        FragmentTransaction fra = transaction.beginTransaction();
        fra.setCustomAnimations(R.animator.fragment_slide_left_enter,
                R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter,
                R.animator.fragment_slide_right_exit);
        fra.replace(R.id.fragment_container, newFragment);
        fra.addToBackStack(TransitionFragment.NAME);
        fra.commit();
        onPause();
    }
    //getting data from Transition Fragment
    @Override
    public void getTransitionTable(ArrayList<String> stateNames, ArrayList<String> inputs, ArrayList<String> toBeMovedState) {
        //setting values
        this.stateNameInTransitions=stateNames;
        this.inputsInTransitions=inputs;
        this.toBeMovedStateInTransitions=toBeMovedState;
        //setting that fragment is opening for first time
        SharedPreferences sp=this.getSharedPreferences(F_NAME,0);
        SharedPreferences.Editor editor=sp.edit();
        String tag= ConversionFragment.NAME;
        editor.putInt(tag+E_PART_B,1);
        editor.commit();
        //Replacing fragment
        ConversionFragment newFragment = new ConversionFragment();
        ConversionFragment.clear(this);
        FragmentManager transaction = getFragmentManager();
        FragmentTransaction fra = transaction.beginTransaction();
        fra.replace(R.id.fragment_container, newFragment);
        fra.setCustomAnimations(R.animator.fragment_slide_left_enter,
                R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter,
                R.animator.fragment_slide_right_exit);
        fra.addToBackStack(ConversionFragment.NAME);
        fra.commit();
        newFragment.setTransition(stateName,isInitial,isFinal,stateNameInTransitions,inputsInTransitions,toBeMovedStateInTransitions,majorType);
        init_FA();
        onPause();
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount()>1) {
            FragmentManager transaction = getFragmentManager();
            transaction.popBackStackImmediate();
        }
        else
            super.onBackPressed();
    }
    //getting data from Conversion Fragment
    @Override
    public void getConversionType(int conversionType)
    {
        init_FA();
        if(this.majorType==0)
        {
            if(conversionType==0)
            {
                f.RECalculator();
            }
            ArrayList<String> stateName = new ArrayList<String>();
            ArrayList<Boolean> isInitial=new ArrayList<Boolean>();
            ArrayList<Boolean> isFinal=new ArrayList<Boolean>();
            ArrayList<String> stateNameInTransitions=new ArrayList<String>();
            ArrayList<String> inputsInTransitions=new ArrayList<String>();
            ArrayList<String> toBeMovedStateInTransitions=new ArrayList<String>();
            //setting that fragment is opening for first time
            SharedPreferences sp=this.getSharedPreferences(F_NAME,0);
            SharedPreferences.Editor editor=sp.edit();
            String tag= ReOutputFragment.NAME;
            editor.putInt(tag+E_PART_B,1);
            editor.commit();
            //Replacing fragment
            f.getOutput(stateName,isInitial,isFinal,stateNameInTransitions,inputsInTransitions,toBeMovedStateInTransitions);
            ReOutputFragment newFragment = new ReOutputFragment();
            newFragment.clear(this);
            newFragment.setOutput(inputsInTransitions);
            FragmentManager transaction = getFragmentManager();
            FragmentTransaction fra = transaction.beginTransaction();
            fra.setCustomAnimations(R.animator.fragment_slide_left_enter,
                    R.animator.fragment_slide_left_exit,
                    R.animator.fragment_slide_right_enter,
                    R.animator.fragment_slide_right_exit);
             fra.replace(R.id.fragment_container, newFragment);
            fra.addToBackStack(ReOutputFragment.NAME);
            fra.commit();
        }
        else {
            if(this.majorType==1)
            {
                if(conversionType==0)
                {
                    f=f.NFAtoDFA();
                    f.RECalculator();
                    ArrayList<String> stateName = new ArrayList<String>();
                    ArrayList<Boolean> isInitial=new ArrayList<Boolean>();
                    ArrayList<Boolean> isFinal=new ArrayList<Boolean>();
                    ArrayList<String> stateNameInTransitions=new ArrayList<String>();
                    ArrayList<String> inputsInTransitions=new ArrayList<String>();
                    ArrayList<String> toBeMovedStateInTransitions=new ArrayList<String>();
                    //setting that fragment is opening for first time
                    SharedPreferences sp=this.getSharedPreferences(F_NAME,0);
                    SharedPreferences.Editor editor=sp.edit();
                    String tag= ReOutputFragment.NAME;
                    editor.putInt(tag+E_PART_B,1);
                    editor.commit();
                    //Replacing fragment
                    f.getOutput(stateName,isInitial,isFinal,stateNameInTransitions,inputsInTransitions,toBeMovedStateInTransitions);
                    ReOutputFragment newFragment = new ReOutputFragment();
                    newFragment.clear(this);
                    newFragment.setOutput(inputsInTransitions);
                    FragmentManager transaction = getFragmentManager();
                    FragmentTransaction fra = transaction.beginTransaction();
                    fra.setCustomAnimations(R.animator.fragment_slide_left_enter,
                            R.animator.fragment_slide_left_exit,
                            R.animator.fragment_slide_right_enter,
                            R.animator.fragment_slide_right_exit);
                    fra.replace(R.id.fragment_container, newFragment);
                    fra.addToBackStack(ReOutputFragment.NAME);
                    fra.commit();
                    return;
                }
                else if(conversionType==1)
                {
                    f=f.NFAtoDFA();
                    type=0;
                }
            }
            else if(this.majorType==2)
            {
                if(conversionType==0)
                {
                    f=f.NFANullToNFA();
                    f=f.NFAtoDFA();
                    f.RECalculator();
                    ArrayList<String> stateName = new ArrayList<String>();
                    ArrayList<Boolean> isInitial=new ArrayList<Boolean>();
                    ArrayList<Boolean> isFinal=new ArrayList<Boolean>();
                    ArrayList<String> stateNameInTransitions=new ArrayList<String>();
                    ArrayList<String> inputsInTransitions=new ArrayList<String>();
                    ArrayList<String> toBeMovedStateInTransitions=new ArrayList<String>();
                    //setting that fragment is opening for first time
                    SharedPreferences sp=this.getSharedPreferences(F_NAME,0);
                    SharedPreferences.Editor editor=sp.edit();
                    String tag= ReOutputFragment.NAME;
                    editor.putInt(tag+E_PART_B,1);
                    editor.commit();
                    //Replacing fragment
                    f.getOutput(stateName,isInitial,isFinal,stateNameInTransitions,inputsInTransitions,toBeMovedStateInTransitions);
                    ReOutputFragment newFragment = new ReOutputFragment();
                    newFragment.clear(this);
                    newFragment.setOutput(inputsInTransitions);
                    FragmentManager transaction = getFragmentManager();
                    FragmentTransaction fra = transaction.beginTransaction();
                    fra.setCustomAnimations(R.animator.fragment_slide_left_enter,
                            R.animator.fragment_slide_left_exit,
                            R.animator.fragment_slide_right_enter,
                            R.animator.fragment_slide_right_exit);
                    fra.replace(R.id.fragment_container, newFragment);
                    fra.addToBackStack(ReOutputFragment.NAME);
                    fra.commit();
                    return;
                }
                else if(conversionType==1)
                {
                    f=f.NFANullToNFA();
                    f=f.NFAtoDFA();
                    type=0;
                }
                else if(conversionType==2)
                {
                    f=f.NFANullToNFA();
                    type=1;
                }
            }
            ArrayList<String> stateName = new ArrayList<String>();
            ArrayList<Boolean> isInitial=new ArrayList<Boolean>();
            ArrayList<Boolean> isFinal=new ArrayList<Boolean>();
            ArrayList<String> stateNameInTransitions=new ArrayList<String>();
            ArrayList<String> inputsInTransitions=new ArrayList<String>();
            ArrayList<String> toBeMovedStateInTransitions=new ArrayList<String>();
            //setting that fragment is opening for first time
            SharedPreferences sp=this.getSharedPreferences(F_NAME,0);
            SharedPreferences.Editor editor=sp.edit();
            String tag= ConversionFragment.NAME;
            editor.putInt(tag+E_PART_B,1);
            editor.commit();
            //Replacing fragment
            f.getOutput(stateName,isInitial,isFinal,stateNameInTransitions,inputsInTransitions,toBeMovedStateInTransitions);
            ConversionFragment newFragment = new ConversionFragment();
            ConversionFragment.clear(this);
            FragmentManager transaction = getFragmentManager();
            FragmentTransaction fra = transaction.beginTransaction();
            fra.replace(R.id.fragment_container, newFragment);
            fra.setCustomAnimations(R.animator.fragment_slide_left_enter,
                    R.animator.fragment_slide_left_exit,
                    R.animator.fragment_slide_right_enter,
                    R.animator.fragment_slide_right_exit);
            fra.addToBackStack(ConversionFragment.NAME);
            fra.commit();

            newFragment.setTransition(stateName,isInitial,isFinal,stateNameInTransitions,inputsInTransitions,toBeMovedStateInTransitions,type);

        }
        onPause();
    }
    //getting data from ReOutput Fragment
    @Override
    public void newFA() {
        //emptying all things
        FragmentManager fm = getFragmentManager();
        SharedPreferences preferences = this.getSharedPreferences(NAME, 0);
        preferences.edit().clear().commit();
        preferences = this.getSharedPreferences(F_NAME, 0);
        preferences.edit().clear().commit();
        FragmentTransaction ft = fm.beginTransaction();
        while (getFragmentManager().getBackStackEntryCount()>0) {
            FragmentManager transaction = getFragmentManager();
            transaction.popBackStackImmediate();
        }
        //Replacing fragment
        InputLanguageFragment homeFragment = new InputLanguageFragment();
        homeFragment.clear(this);
        ft.setCustomAnimations(R.animator.fragment_slide_left_enter,
                R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter,
                R.animator.fragment_slide_right_exit);
        ft.replace(R.id.fragment_container, homeFragment);
        ft.addToBackStack("Inputs");
        ft.commit();

        f=new FA();
    }
    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btn_info)
        {
            startActivityForResult(new Intent(this,InfoActivity.class),0);
        }
    }

    public void init_FA() {
        f=new FA();
        if(stateName!=null&&stateNameInTransitions!=null) {
            for (int i = 0; i < stateName.size(); i++) {
                f.addState(stateName.get(i), isInitial.get(i), isFinal.get(i));
            }
            for (int i = 0; i < inputLanguage.size(); i++) {
                f.addInputLanguage(inputLanguage.get(i));
            }
            for (int i = 0; i < inputsInTransitions.size(); i++) {
                f.addTransition(stateNameInTransitions.get(i), toBeMovedStateInTransitions.get(i), inputsInTransitions.get(i));
            }
        }
    }
    public void save(SharedPreferences dataStore){
        SharedPreferences.Editor editor = dataStore.edit();
        editor.putInt("stateSize",stateName.size());
        for (int i=0;i<stateName.size();i++) {
            editor.putString("stateList" + i, stateName.get(i));
            editor.putBoolean("initialStateList" + i, isInitial.get(i));
            editor.putBoolean("finalStateList" + i, isFinal.get(i));
        }
        editor.putInt("languageSize",inputLanguage.size());
        for (int i=0;i<inputLanguage.size();i++) {
            editor.putString("inputLanguage" + i, inputLanguage.get(i));
        }
        editor.putInt("TransSize",stateNameInTransitions.size());
        for (int i=0;i<stateNameInTransitions.size();i++) {
            editor.putString("stateNameInTransitions" + i, stateNameInTransitions.get(i));
            editor.putString("inputsInTransitions" + i, inputsInTransitions.get(i));
            editor.putString("toBeMovedStateInTransitions" + i, toBeMovedStateInTransitions.get(i));
        }
        editor.putInt("type",majorType);
        editor.putInt("type1",type);

        editor.commit();
    }
    public void load(SharedPreferences dataStore){
        if(stateName==null||stateName.size()==0){
            int counter = dataStore.getInt("stateSize",0);
            stateName=new ArrayList<>();
            isFinal=new ArrayList<>();
            isInitial=new ArrayList<>();
            for (int i = 0; i < counter; i++)
            {
                stateName.add(dataStore.getString("stateList" + i, ""));
                isInitial.add(dataStore.getBoolean("initialStateList" + i, false));
                isFinal.add(dataStore.getBoolean("finalStateList" + i, false));
            }
        }
        if(inputLanguage==null||inputLanguage.size()==0){
            int counter = dataStore.getInt("languageSize",0);
            inputLanguage=new ArrayList<>();
            for (int i = 0; i < counter; i++)
            {
                inputLanguage.add(dataStore.getString("inputLanguage" + i, ""));
            }
        }
        if(stateNameInTransitions==null||stateNameInTransitions.size()==0){
            int counter = dataStore.getInt("TransSize",0);
            stateNameInTransitions=new ArrayList<>();
            inputsInTransitions=new ArrayList<>();
            toBeMovedStateInTransitions=new ArrayList<>();
            for (int i = 0; i < counter; i++)
            {
                stateNameInTransitions.add(dataStore.getString("stateNameInTransitions" + i, ""));
                inputsInTransitions.add(dataStore.getString("inputsInTransitions" + i, ""));
                toBeMovedStateInTransitions.add(dataStore.getString("toBeMovedStateInTransitions" + i, ""));
            }
        }
        init_FA();
        majorType=dataStore.getInt("type",0);
    }

}
