package com.abdul.automata.FA_Engine;

import java.util.ArrayList;

/**
 * Created by Abdul Rehman on 10/6/2016.
 */
public class MultipleState extends State{
    ArrayList<State> states;
    public MultipleState(String stateName, boolean isInitial, boolean isFinal) {
        super(stateName, isInitial, isFinal);
    }
    public MultipleState() {
        super();
        states=new ArrayList<State>();
        inComing=new ArrayList<TransitionIn>();
        outGoing= new ArrayList<TransitionOut>();
    }
    @Override
    public void addState(State s)
    {
        states.add(s);
    }
    public ArrayList<State> getNextStates(String input)
    {
        ArrayList<State> list=new ArrayList<State>();
        for (int i=0;i<states.size();i++)
        {
            ArrayList<State> check=states.get(i).getNextStates(input);
            for (int j=0;j<check.size();j++){
                if(!list.contains(check.get(j)))
                {
                    list.add(check.get(j));
                }
            }
        }
        return  list;
    }
}
