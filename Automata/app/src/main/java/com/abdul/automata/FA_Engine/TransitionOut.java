package com.abdul.automata.FA_Engine;

/**
 * Created by Abdul Rehman on 10/6/2016.
 */
public class TransitionOut {
    String input;
    State nextState;

    public String getInput() {
        return input;
    }

    public State getNextState() {
        return nextState;
    }

    public void setNextState(State nextState) {
        this.nextState = nextState;
    }

    public TransitionOut(String input) {
        this.input = input;
    }
}
